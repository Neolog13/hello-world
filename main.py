import telebot
import webbrowser
from telebot import types
from config import TOKEN_BOT

bot = telebot.TeleBot(TOKEN_BOT)


@bot.message_handler(commands=['start'])
def start(message):
    markup = types.ReplyKeyboardMarkup()
    btn1 = types.KeyboardButton('Перейти на сайт')
    markup.row(btn1)
    btn2 = types.KeyboardButton('Удалить фото')
    btn3 = types.KeyboardButton('Изменить текст')
    markup.row(btn2, btn3)
    file = open('./photo.png', 'rb')
    bot.send_photo(message.chat.id, file, reply_markup=markup)
    #bot.send_message(message.chat.id, 'Привет', reply_markup=markup)
    bot.register_next_step_handler(message, on_click)

def on_click(message):
    if message.text == 'Перейти на сайт':
        webbrowser.open('https://www.google.com/')
    elif message.text == 'Удалить фото':
        bot.send_message(message.chat.id, 'Photo deleted')

@bot.message_handler(content_types=['photo'])
def get_photo(message):
    markup = types.InlineKeyboardMarkup()
    btn1 = types.InlineKeyboardButton('Перейти на сайт', url='https://google.com')
    markup.row(btn1)
    btn2 = types.InlineKeyboardButton('Удалить фото', callback_data='delete')
    btn3 = types.InlineKeyboardButton('Изменить текст', callback_data='edit')
    markup.row(btn2, btn3)
    #markup.add()
    # markup.add(types.InlineKeyboardButton('Удалить фото', callback_data='delete'))
    # markup.add(types.InlineKeyboardButton('Изменить текст', callback_data='edit'))
    bot.reply_to(message, 'Какое красивое фото', reply_markup=markup)


@bot.callback_query_handler(func=lambda callback: True)
def callback_message(callback):
    if callback.data == 'delete':
        bot.delete_message(callback.message.chat.id, callback.message.message_id - 1)
    elif callback.data == 'edit':
        bot.edit_message_text('Edit text', callback.message.chat.id, callback.message.message_id)

@bot.message_handler(commands=['site,' 'website'])
def site(message):
    webbrowser.open('https://www.google.com/')


# @bot.message_handler(commands=['start', 'main', 'hello'])
# def main(message):
#     bot.send_message(message.chat.id, 'Привет покупатель фетровых игрушек. Какую игрушку ты хочешь купить?')
#     #bot.send_message(message.chat.id, f'Привет {message.from_user.username}')


@bot.message_handler(commands=['help'])
def main(message):
    bot.send_message(message.chat.id, '<b>Че случилось?</b> <em><u>Но ваще помощи ждать неоткуда</u></em>', parse_mode='html')


@bot.message_handler()
def info(message):
    if message.text.lower() == 'привет':
        bot.send_message(message.chat.id, f'Привет {message.from_user.username}')
    elif message.text.lower() == 'id':
        bot.reply_to(message, f'ID: {message.from_user.id}')
#    elif message.text.lower() == '/start':
#        bot.send_message(message.chat.id, 'Привет покупатель фетровых игрушек. Какую игрушку ты хочешь купить?')
#

bot.polling(none_stop=True)
#bot.infinity_polling() бесконечная работа