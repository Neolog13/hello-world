import telebot
import requests
import json
from config import TOKEN_BOT, WEATHER_API


bot = telebot.TeleBot(TOKEN_BOT)
API = WEATHER_API

@bot.message_handler(commands=['start'])
def start(message):
    bot.send_message(message.chat.id, 'Привет, рад тебя видеть! Напиши название города')


@bot.message_handler(content_types=['text'])
def get_weather(message):
    city = message.text.strip().lower()
    res = requests.get(f'https://api.openweathermap.org/data/2.5/weather?q={city}&appid={API}&units=metric')
    if res.status_code == 200:
        data = json.loads(res.text)
        bot.reply_to(message, f'Сейчас погода: {data["main"]["temp"]}')
        data1 = data["weather"][0]["icon"]
        img = requests.get(f'https://openweathermap.org/img/wn/{data1}@2x.png', stream=True)
        bot.reply_to(message, f'https://openweathermap.org/img/wn/{data1}@2x.png')
        with open(f'{data1}@2x.png', 'wb') as image:
            bot.send_photo(message.chat.id, image)




    else:
        bot.reply_to(message, f'Город указан не верно')




bot.polling(non_stop=True)